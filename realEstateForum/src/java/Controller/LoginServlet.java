/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Json.JwtToken;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author OS
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        try (PrintWriter out = response.getWriter()) {
            String idToken = request.getParameter("idtoken");
            System.out.println(idToken);
            if(idToken != null)
            {
                String[] chunks = idToken.split("\\.");
                Base64.Decoder decoder = Base64.getUrlDecoder();

                String header = new String(decoder.decode(chunks[0]));
                String payload = new String(decoder.decode(chunks[1]));
                System.out.println("Payload: " + payload);
                JwtToken token = new JwtToken(payload);
                System.out.println("Token: " + token.getJsonMap().toString());
                System.out.println("Sub: " + token.getJsonMap().get("sub"));
                System.out.println("Hd: " + token.getJsonMap().get("hd"));
                System.out.println("email: " + token.getJsonMap().get("email"));
                System.out.println("email_verified: " + token.getJsonMap().get("email_verified"));
                System.out.println("name: " + token.getJsonMap().get("name"));
                System.out.println("exp: " + token.getJsonMap().get("exp"));
                int dateExp = Integer.parseInt(token.getJsonMap().get("exp").toString());

                url="Success.jsp";  
                
            }
            else
            {
            String name = request.getParameter("userName");
            String pass = request.getParameter("pass");
            if(name.equals("123") && pass.equals("123"))
            {
                url="Success.jsp";
                System.out.println("Login success");
                request.getRequestDispatcher(url).forward(request, response);
                
            }
            }
            
        } catch(IOException | ServletException e){
        }
    }
        
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
