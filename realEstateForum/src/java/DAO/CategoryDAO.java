/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import model.Category;
import model.Comment;
import model.DatabaseInfo;

/**
 *
 * @author OS
 */
public class CategoryDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    public Category GetCategoryById(String id)
    {
        String SELECT_CATEGORY= "SELECT * FROM Category WHERE category_id = ?";

        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(SELECT_CATEGORY);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new Category(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3)
                );
            }
            conn.close();
            pstmt.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    
    public String CreateCategory(Category category){
        String CREATE_CATEGORY = "INSERT INTO Category VALUES (?,?,?) output Inserted.category_id";
        String id = null;
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(CREATE_CATEGORY);
            pstmt.setString(1, category.getId());
            pstmt.setString(2, category.getName());
            pstmt.setString(3, category.getType());
            rs = pstmt.executeQuery();
            while(rs.next()){
                id =rs.getString("category_id");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }
    
    public String UpdateCategory(Category category){
        String UPDATE_CATEDORY = "Update Category set category_id = ?,category_name = ?, category_type = ?, where category_id = ? output inserted.category_id";
        String id = null;
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(UPDATE_CATEDORY);
            pstmt.setString(1, category.getId());
            pstmt.setString(2, category.getName());
            pstmt.setString(3, category.getType());
            while(rs.next()){
                id =rs.getString("category_id");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }
    
    public String DeleteCategory(String id){
        String DELETE_CATEGORY = "Delete from Category where category_id = ? output deleted.category_id";
        String deletedid = null;
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(DELETE_CATEGORY);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                deletedid =rs.getString("category_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return deletedid;
    }
}
