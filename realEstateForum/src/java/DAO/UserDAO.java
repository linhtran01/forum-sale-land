/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Account;
import model.DatabaseInfo;
import static model.DatabaseInfo.getConnect;
import model.User;
//a
/**
 *
 * @author PHONG VU
 */
public class UserDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;

    // list
    public List<User> getAllUser() {
        List<User> list = new ArrayList<>();
        String SELECT_ALL_USER = "SELECT * FROM USER;";

        try {
            conn = getConnect();
            pstmt = conn.prepareStatement(SELECT_ALL_USER);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                list.add(new User (
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("email"),
                        rs.getString("address"),
                        new Date(rs.getDate("date").getTime()),
                        getAccounUsername(rs.getString("username"))
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    
    // add
    public String addUser(String name, String phone, String email, String address, Date date, String username ) {
        String INSERT_USER = "INSERT INTO User values (?,?,?,?,?,?,?)";
        try {
            conn = getConnect();
            pstmt = conn.prepareStatement(INSERT_USER);
            
            pstmt.setString(1, name);
            pstmt.setString(2, phone);
            pstmt.setString(3, email);
            pstmt.setString(4, address);
            pstmt.setDate(5, new java.sql.Date(date.getTime()));
            pstmt.setString(6, username);
            
            pstmt.executeUpdate();
            
        } catch(SQLException e) {
            System.err.println("Error when inserting!!");
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    // update 
    public void updateUser(String id, String name, String phone, String email, String address, Date date, String username) {
        String UPDATE_USER = "UPDATE User set "
                + "name = ?,"
                + "phone = ?,"
                + "email = ?,"
                + "address = ?,"
                + "date = ?,"
                + "username = ? "
                + "where id = ?";
        try {
            conn = getConnect();
            pstmt = conn.prepareStatement(UPDATE_USER);
            
            pstmt.setString(1, name);
            pstmt.setString(2, phone);
            pstmt.setString(3, email);
            pstmt.setString(4, address);
            pstmt.setDate(5, new java.sql.Date(date.getTime()));
            pstmt.setString(6, username);
            pstmt.setString(7, id);
            
            pstmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    // delete
    public void deleteUser(String id) {
        String DELETE_USER = "DELETE FROM User WHERE id = ?;";
        
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(DELETE_USER);
            pstmt.setString(1, id);
            pstmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    // get id
    public User getUserById(String id) {
        String GET_USER_ID = "Select * from User Where id = ?";
        
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(GET_USER_ID);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
                return new User(
                        rs.getString("id"),
                        rs.getString("name"),
                        rs.getString("phone"),
                        rs.getString("email"),
                        rs.getString("address"),
                        new Date(rs.getDate("date").getTime()),
                        getAccounUsername(rs.getString("username"))
                );
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                conn.close();
            pstmt.close();
            rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static Account getAccounUsername(String username) {
        String SELECT_ACOUNT = "SELECT * FROM ACCOUNT WHERE username = ?";

        try {
            Connection connection = DatabaseInfo.getConnect();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACOUNT);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new Account(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                );
            }
            connection.close();
            preparedStatement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}