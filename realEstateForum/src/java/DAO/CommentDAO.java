/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Comment;
import model.DatabaseInfo;

/**
 *
 * @author OS
 */
public class CommentDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    public List<Comment> GetAllCommentByPostID(String postID) {
        List<Comment> CommentList = new ArrayList<>();
        String SELECT_ALL_COMMENT = "Select * From Comment where post_id = ?";
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(SELECT_ALL_COMMENT);
            pstmt.setString(1, postID);
            rs = pstmt.executeQuery();
            while (rs.next()) {                
                CommentList.add(new Comment(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        new Date(rs.getDate(4).getTime()),
                        rs.getString(5)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return CommentList;
    }
    
    public Comment GetCommentById(String id)
    {
        String SELECT_COMMENT= "SELECT * FROM Comment WHERE comment_id = ?";

        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(SELECT_COMMENT);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return new Comment(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        new Date(rs.getDate(4).getTime()),
                        rs.getString(5)
                );
            }
            conn.close();
            pstmt.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    
    public String CreateComment(Comment comment){
        String CREATE_COMMENT = "INSERT INTO Comment VALUES (?,?,?,?,?) output Inserted.comment_id";
        String id = null;
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(CREATE_COMMENT);
            pstmt.setString(1, comment.getId());
            pstmt.setString(2, comment.getPost_id());
            pstmt.setString(3, comment.getContent());
            pstmt.setDate(4, new java.sql.Date(comment.getDate().getTime()));
            pstmt.setString(5, comment.getUsername());
            rs = pstmt.executeQuery();
            while(rs.next()){
                id =rs.getString("comment_id");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        
        return id;
    }
    
    public String UpdateComment(Comment comment){
        String UPDATE_COMMENT = "Update Comment set comment_id = ?, post_id = ?,content = ?, date = ?, username = ? where comment_id = ? output inserted.comment_id";
        String id = null;
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(UPDATE_COMMENT);
            pstmt.setString(1, comment.getId());
            pstmt.setString(2, comment.getPost_id());
            pstmt.setString(3, comment.getContent());
            pstmt.setDate(4, new java.sql.Date(comment.getDate().getTime()));
            pstmt.setString(5, comment.getUsername());
            pstmt.setString(6, comment.getId());
            while(rs.next()){
                id =rs.getString("comment_id");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return id;
    }
    
    public String DeleteComment(String id){
        String DELETE_COMMENT = "Delete from Comment where comment_id = ? output deleted.comment_id";
        String deletedid = null;
        try {
            conn = DatabaseInfo.getConnect();
            pstmt = conn.prepareStatement(DELETE_COMMENT);
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            
            while(rs.next()){
                deletedid =rs.getString("comment_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                pstmt.close();
                rs.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
        return deletedid;
    }
}
