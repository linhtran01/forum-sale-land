/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author OS
 */
public class Comment {
    private String id;
    private String post_id;
    private String content;
    private Date date;
    private String username;

    public Comment() {
    }

    public Comment(String id, String post_id, String content, Date date, String username) {
        this.id = id;
        this.post_id = post_id;
        this.content = content;
        this.date = date;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Comment{" + "id=" + id + ", post_id=" + post_id + ", content=" + content + ", date=" + date + ", username=" + username + '}';
    }
    
}
