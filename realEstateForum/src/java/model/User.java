/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author OS
 */
public class User {
    private String id;
    private String name;
    private String phone;
    private String email;
    private String address;
    private Date date;
    private Account account;

    public User() {
    }

    public User(String id, String name, String email, Date date, Account account) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.date = date;
        this.account = account;
    }

    public User(String id, String name, String phone, String email, Date date, Account account) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.date = date;
        this.account = account;
    }

    public User(String id, String name, String phone, String email, String address, Date date, Account account) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.date = date;
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Account getAccount() {
        return account;
    }

    public void setUsername(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", phone=" + phone + ", email=" + email + ", address=" + address + ", date=" + date + ", account=" + account + '}';
    }

    
    
}
