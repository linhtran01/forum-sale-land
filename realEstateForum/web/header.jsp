<%-- 
    Document   : header
    Created on : Mar 12, 2022, 9:53:54 AM
    Author     : OS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./css/style.css" />
</head>
<body>
  <a href="#" class="to-top">TOP</a>
  <div class="container-ratio header">
    <div class="overlay"></div>
    <div class="ratio">
      <img src="./image/image-topsearch-bg_01-1024x310.jpg" alt="bg">
    </div>
    <div class="header-box">
      <div class="d-between nav-bar">
        <div class="logo">
          <img src="./image/logo-white-2.png" alt="logo">
        </div>
        <div class="menu">
          <ul class="d-center">
            <li class="menu-item active"><a href="#">Trang chủ</a></li>
            <li class="menu-item"><a href="#">Giới thiệu</a></li>
            <li class="menu-item"><a href="#">Thuê Nhà</a></li>
            <li class="menu-item"><a href="#">Dự án</a></li>
            <li class="menu-item"><a href="#">Tin tức</a></li>
            <li class="menu-item"><a href="#">Liên hệ</a></li>
            <li class="menu-item"><a class="button-red" href="#">Gửi bán và cho thuê</a></li>
            <li class="menu-item"><a class="button-white" href="#">Liên hệ ngay</a></li>
          </ul>
        </div>
      </div>
      <div class="banner">
        <form>
          <label class="search-title" for="search">Tìm kiếm ngôi nhà bạn yêu thích</label>
          <div class="search-input">
            <input type="text" placeholder="Nhập địa điểm, chung cư, tòa nhà...." id="search">
            <input class="button-red" type="submit" value="Tìm kiếm">
          </div>
        </form>
      </div>
    </div>
  </div>