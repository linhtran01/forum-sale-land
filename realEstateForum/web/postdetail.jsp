<%-- 
    Document   : postdetail
    Created on : 17-03-2022, 16:42:29
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v13.0" nonce="xD4IUnEy"></script>
<script type="text/javascript" src="js/jquery-1.6.2.js"></script>
<div id="fb-root"></div>
  <section class="post-container">
    <div class="title">
      <h2 class="post-title">DỰ ÁN MỚI</h2>
      <p class="sub-title">
        Tổng hợp những mẫu dự án hot nhất trên thị trường bất động sản hiện nay. 
        Liên hệ hotline 0962 682 434 để được hỗ trợ mua nhà nhanh nhất
      </p>
    </div>
    <div class="post-list">
        <c:forEach items="${listPost}" begin="0" end="5" var="post">
            <div class="post-item">
                <div class="post-item-image">
                  <a href="listPost?pId=${post.id}&action=show">
                    <div class="container-ratio">
                      <div class="ratio">
                        <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
                      </div>
                    </div>
                    <div class="discount">
                      Thanh toán 30% nhận nhà
                    </div>
                    <div class="saling">
                      Đang mở bán
                    </div>
                  </a>
                </div>
                <div class="post-infor">
                  <h4 class="post-name">
                    <a href="#">
                      ${post.post_title}
                    </a>
                  </h4>
                  <p class="post-location">
                    Đào Duy Anh, Gò Vấp, TPHCM
                  </p>
                  <div class="d-between post-price">
                    <div class="price">Giá: <span>${post.price} tr/m2</span></div>
                    <div class="time">SDT: <span>${post.phone_contact}</span></div>
                  </div>
                </div>
              </div>
        </c:forEach>
    </div>
         <!--<form name="details" method="post" action="CommentServlet"-->
                <!--onReset="return confirm('Do you really want to clear the form and start again?')"-->
             <!--<p>Comment:<br>
                <textarea id="content" name="comment" rows="3" cols="50" wrap="virtual"></textarea>
            </p>
            <p>
                <input id ="btnComment" type="submit" value="Submit" name="Comment">
                <input type="reset" value="Clear" >
            </p>
            <c:forEach items="${listPost}" begin="0" end="5" var="post">
                 <span>${post.getUsername()}</span>
                 <span>${post.getCmt()}</span>
            </c:forEach>
            <!--</form>-->
            <span id="result1"></span>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#btnComment').click(function () {
                        var comment = $('#content').val();
                        $.ajax({
                            type: 'POST',
                            data: {comment: comment},
                            url: 'CommentServlet',
                            success: function (result) {
                                $('#result1').html(result);
                            }
                        });
                    });
                });
            </script>   
            <div class="fb-comments" data-href="https://batdongsan.com.vn/" data-width="1000" data-numposts="5"></div>
  </section>
  <%@ include file="footer.jsp" %>
