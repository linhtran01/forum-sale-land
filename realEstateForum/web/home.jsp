<%-- 
    Document   : home
    Created on : Mar 12, 2022, 9:54:47 AM
    Author     : OS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="header.jsp" %>
  <section class="post-container">
    <div class="title">
      <h2 class="post-title">DỰ ÁN MỚI</h2>
      <p class="sub-title">
        Tổng hợp những mẫu dự án hot nhất trên thị trường bất động sản hiện nay. 
        Liên hệ hotline 0962 682 434 để được hỗ trợ mua nhà nhanh nhất
      </p>
    </div>
    <div class="post-list">
      <div class="post-item">
        <div class="post-item-image">
          <a href="#">
            <div class="container-ratio">
              <div class="ratio">
                <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
              </div>
            </div>
            <div class="discount">
              Thanh toán 30% nhận nhà
            </div>
            <div class="saling">
              Đang mở bán
            </div>
          </a>
        </div>
        <div class="post-infor">
          <h4 class="post-name">
            <a href="#">
              Sunshine Gardent City
            </a>
          </h4>
          <p class="post-location">
            Đào Duy Anh, Gò Vấp, TPHCM
          </p>
          <div class="d-between post-price">
            <div class="price">Giá: <span>53-62 tr/m2</span></div>
            <div class="time">Giao nhà: <span>03/2025</span></div>
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="post-item-image">
          <a href="#">
            <div class="container-ratio">
              <div class="ratio">
                <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
              </div>
            </div>
            <div class="discount">
              Thanh toán 30% nhận nhà
            </div>
            <div class="saling">
              Đang mở bán
            </div>
          </a>
        </div>
        <div class="post-infor">
          <h4 class="post-name">
            <a href="#">
              Sunshine Gardent City
            </a>
          </h4>
          <p class="post-location">
            Đào Duy Anh, Gò Vấp, TPHCM
          </p>
          <div class="d-between post-price">
            <div class="price">Giá: <span>53-62 tr/m2</span></div>
            <div class="time">Giao nhà: <span>03/2025</span></div>
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="post-item-image">
          <a href="#">
            <div class="container-ratio">
              <div class="ratio">
                <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
              </div>
            </div>
            <div class="discount">
              Thanh toán 30% nhận nhà
            </div>
            <div class="saling">
              Đang mở bán
            </div>
          </a>
        </div>
        <div class="post-infor">
          <h4 class="post-name">
            <a href="#">
              Sunshine Gardent City
            </a>
          </h4>
          <p class="post-location">
            Đào Duy Anh, Gò Vấp, TPHCM
          </p>
          <div class="d-between post-price">
            <div class="price">Giá: <span>53-62 tr/m2</span></div>
            <div class="time">Giao nhà: <span>03/2025</span></div>
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="post-item-image">
          <a href="#">
            <div class="container-ratio">
              <div class="ratio">
                <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
              </div>
            </div>
            <div class="discount">
              Thanh toán 30% nhận nhà
            </div>
            <div class="saling">
              Đang mở bán
            </div>
          </a>
        </div>
        <div class="post-infor">
          <h4 class="post-name">
            <a href="#">
              Sunshine Gardent City
            </a>
          </h4>
          <p class="post-location">
            Đào Duy Anh, Gò Vấp, TPHCM
          </p>
          <div class="d-between post-price">
            <div class="price">Giá: <span>53-62 tr/m2</span></div>
            <div class="time">Giao nhà: <span>03/2025</span></div>
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="post-item-image">
          <a href="#">
            <div class="container-ratio">
              <div class="ratio">
                <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
              </div>
            </div>
            <div class="discount">
              Thanh toán 30% nhận nhà
            </div>
            <div class="saling">
              Đang mở bán
            </div>
          </a>
        </div>
        <div class="post-infor">
          <h4 class="post-name">
            <a href="#">
              Sunshine Gardent City
            </a>
          </h4>
          <p class="post-location">
            Đào Duy Anh, Gò Vấp, TPHCM
          </p>
          <div class="d-between post-price">
            <div class="price">Giá: <span>53-62 tr/m2</span></div>
            <div class="time">Giao nhà: <span>03/2025</span></div>
          </div>
        </div>
      </div>
      <div class="post-item">
        <div class="post-item-image">
          <a href="#">
            <div class="container-ratio">
              <div class="ratio">
                <img src="./image/The-Grand-Mahattan-can-ho-mau-3_1555820433.jpg" alt="land sale">
              </div>
            </div>
            <div class="discount">
              Thanh toán 30% nhận nhà
            </div>
            <div class="saling">
              Đang mở bán
            </div>
          </a>
        </div>
        <div class="post-infor">
          <h4 class="post-name">
            <a href="#">
              Sunshine Gardent City
            </a>
          </h4>
          <p class="post-location">
            Đào Duy Anh, Gò Vấp, TPHCM
          </p>
          <div class="d-between post-price">
            <div class="price">Giá: <span>53-62 tr/m2</span></div>
            <div class="time">Giao nhà: <span>03/2025</span></div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <%@ include file="footer.jsp" %>